Basic Overview of Convolutional Neural Network (CNN)

Convolutional Neural Network is a class of deep neural network that is used for Computer Vision or analyzing visual imagery.
Convolutional Layer


Computers read images as pixels and it is expressed as a matrix (NxNx3) — (height by width by depth). Images make use of three channels (RGB), so that is why we have a depth of 3.


The Convolutional Layer makes use of a set of learnable filters.


A filter is used to detect the presence of specific features or patterns present in the original image (input).


This filter is convolved (slided) across the width and height of the input file, and a dot product is computed to give an activation map.




Activation Function

The activation function is a node that is put at the end of or in between Neural Networks. They help to decide if the neuron would fire or not.

Pooling Layer


The Pooling layer can be seen between Convolution layers in a CNN architecture.


This layer basically reduces the number of parameters and computation in the network, controlling overfitting by progressively reducing the spatial size of the network.


Fully-connected Layer


In this layer, the neurons have a complete connection to all the activations from the previous layers.


Their activations can hence be computed with a matrix multiplication followed by a bias offset.


This is the last phase for a CNN network.


The Convolutional Neural Network is actually made up of hidden layers and fully-connected layer(s).
